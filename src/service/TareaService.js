import Axios from 'axios';

export class TareaService {
    baseUrl = "http://localhost:8080/";
    getAll() {
        return Axios.get(this.baseUrl + "tareas").then(
            res => res.data
        );
    }

    save(tarea) {
        return Axios.put(this.baseUrl + "tarea", tarea).then(res => res.data);
    }

    delete(id) {
        return Axios.delete(this.baseUrl + "tarea/"+id).then(res => res.data);
    }
}