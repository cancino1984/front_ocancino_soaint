import './App.css';
import { Component } from 'react';
import { TareaService } from './service/TareaService';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Panel } from 'primereact/panel';
import { Checkbox } from 'primereact/checkbox';
import { Menubar } from 'primereact/menubar';
import { Dialog } from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';


import 'primereact/resources/themes/nova/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

export default class App extends Component {
    constructor() {
        super();
	    this.state = {
	      visible : false,
	      tarea: {
	        id: null,
	        descripcion: null,
	        fecha: null,
	        vigente: null
	      },
	      selectedTarea : {
	
	      }
	    };
		this.items = [
			{
				label:'Nuevo',
				icon:'pi pi-fw pi-plus',
				command : () => {this.showSaveDialog()}
			},
			{
				label:'Editar',
				icon:'pi pi-fw pi-pencil',
				command : () => {this.showEditDialog()}
			},
			{
				label:'Eliminar',
				icon:'pi pi-fw pi-trash',
        		command : () => {this.delete()}
			}
		];
        this.tareaService = new TareaService();
	    this.save = this.save.bind(this);
	    this.delete = this.delete.bind(this);
	    this.footer = (
	      <div>
	        <Button label="Guardar" icon="pi pi-check" onClick={this.save} />
	      </div>
	    );
    }
    componentDidMount() {
        this.tareaService.getAll().then(data => this.setState({tareas: data}));
    }

  save() {
    this.tareaService.save(this.state.tarea).then(data => {
      this.setState({
        visible : false,
        tarea: {
          id: null,
          descripcion: null,
          fecha: null,
          vigente: null
        }
      });
		alert("Registo guardado!");
      this.tareaService.getAll().then(data => this.setState({tareas: data}))
    })
}

  delete() {
    if(window.confirm("¿Seguro?")) {
      this.tareaService.delete(this.state.selectedTarea.id).then(data => {
		alert("Registo Eliminado!");
        this.tareaService.getAll().then(data => this.setState({tareas: data}));
      });
    }
}

    statusBodyTemplate(rowData) {
        return <Checkbox checked={rowData.vigente}></Checkbox>;
		//<input type="checkbox" checked={rowData.vigente}/>;
    }

    render() {
        return (
			<div style={{width:'80%', margin:'0 auto', marginTop:'20px'}}>
			<Menubar model={this.items}/><br/>
				<Panel header="Tareas">
						<DataTable value={this.state.tareas} selectionMode="single" selection={this.state.selectedTarea} onSelectionChange={e => this.setState({selectedTarea: e.value})}>
							<Column field="id" header="ID"></Column>
							<Column field="descripcion" header="Descripción"></Column>
							<Column field="fecha" header="Fecha Creación"></Column>
							<Column field="vigente" header="Vigente" body={this.statusBodyTemplate}></Column>
						</DataTable>
				</Panel>
				<Dialog header="Agregar Tarea" visible={this.state.visible} style={{width: '40%'}} footer={this.footer} modal={true} onHide={() => this.setState({visible: false})}>
					<form id="tarea-form">
		              <span className="p-float-label">
		                <InputText value={this.state.tarea.descripcion} style={{width : '80%'}} id="descripcion" onChange={(e) => {
		                    let val = e.target.value;
		                    this.setState(prevState => {
		                        let tarea = Object.assign({}, prevState.tarea);
		                        tarea.descripcion = val;
		                        return { tarea };
		                    })}
		                  } />
		                <label htmlFor="descripcion">Descripción</label>
		              </span>
		              <br/>
		              <span>
		                <Checkbox checked={this.state.tarea.vigente} id="vigente" onChange={(e) => {
		                    let val = e.target.checked;
		                    this.setState(prevState => {
		                        let tarea = Object.assign({}, prevState.tarea);
		                        tarea.vigente = val;
		
		                        return { tarea };
		                    })}
		                  } />&nbsp;
		                <label htmlFor="vigente">Vigente</label>
		              </span>
		              <br/>
					</form>
				</Dialog>
			</div>
		);
    }

  showSaveDialog(){
    this.setState({
      visible : true,
      tarea : {
        id: null,
        descripcion: null,
        fecha: null,
        vigente: true
      }
    });
  }

  showEditDialog() {
    this.setState({
      visible : true,
      tarea : {
        id: this.state.selectedTarea.id,
        descripcion: this.state.selectedTarea.descripcion,
        fecha: this.state.selectedTarea.fecha,
        vigente: this.state.selectedTarea.vigente
      }
    })
  }

}